FROM openjdk:8-jdk-alpine

MAINTAINER Alexis Fontaine

ENV MAVEN_VERSION 3.3.9
ENV NODE_VERSION 6.9.1
ENV NPM_VERSION 3

#
# Maven related
#
ENV MAVEN_ROOT /var/lib/maven
ENV MAVEN_HOME ${MAVEN_ROOT}/apache-maven-${MAVEN_VERSION}
ENV MAVEN_OPTS -Xms256m -Xmx512m

RUN echo "# Installing Maven " && echo ${MAVEN_VERSION} && \
    wget -q -O /tmp/apache-maven-${MAVEN_VERSION}.tar.gz \
        http://archive.apache.org/dist/maven/maven-3/${MAVEN_VERSION}/binaries/apache-maven-${MAVEN_VERSION}-bin.tar.gz && \
    mkdir -p ${MAVEN_ROOT} && \
    tar xzf /tmp/apache-maven-${MAVEN_VERSION}.tar.gz -C ${MAVEN_ROOT} && \
    ln -s $MAVEN_HOME/bin/mvn /usr/local/bin && \
    rm -f /tmp/apache-maven-${MAVEN_VERSION}.tar.gz

VOLUME /var/lib/maven

#
# Node.js related
#
RUN apk add --no-cache --virtual build-deps binutils-gold curl g++ gcc gnupg libgcc linux-headers make python git && \
    gpg --keyserver ha.pool.sks-keyservers.net --recv-keys \
        9554F04D7259F04124DE6B476D5A82AC7E37093B \
        94AE36675C464D64BAFA68DD7434390BDBE9B9C5 \
        0034A06D9D9B0064CE8ADF6BF1747F4AD2306D93 \
        FD3A5288F042B6850C66B31F09FE44734EB7990E \
        71DCFD284A79C3B38668286BC97EC7A07EDE3FC1 \
        DD8F2338BAE7501E3DD5AC78C273792F7D83545D \
        C4F0DFFF4E8C1A8236409D08E73BC641CC11F4C8 \
        B9AE9905FFD7803F25714661B63B535A4C206CA9 && \
    curl -SLO https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}.tar.xz && \
    curl -SLO https://nodejs.org/dist/v${NODE_VERSION}/SHASUMS256.txt.asc && \
    gpg --verify SHASUMS256.txt.asc && \
    grep node-v${NODE_VERSION}.tar.xz SHASUMS256.txt.asc | sha256sum -c - && \
    tar -xf node-v${NODE_VERSION}.tar.xz && \
    cd node-v${NODE_VERSION} && \
    export GYP_DEFINES="linux_use_gold_flags=0" && \
    ./configure --prefix=/usr && \
    make -j$(getconf _NPROCESSORS_ONLN) && \
    make install && \
    npm install -g npm@${NPM_VERSION} && \
    npm install -g gulp-cli bower && \
    cd .. && \
    find /usr/lib/node_modules -name test -o -name .bin -type d | xargs rm -rf && \
    apk del build-deps && \
    rm -rf /node-v${NODE_VERSION}.tar.xz /SHASUMS256.txt.asc /node-v${NODE_VERSION} /usr/include \
        /usr/share/man /tmp/* /var/cache/apk/* /root/.npm /root/.node-gyp /root/.gnupg \
        /usr/lib/node_modules/npm/man /usr/lib/node_modules/npm/doc /usr/lib/node_modules/npm/html

#
# Build related
#
RUN apk add --no-cache git build-base nasm autoconf automake zlib-dev python && \
    rm -rf /var/cache/apk/*
